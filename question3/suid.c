#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>


int main(int argc, char *argv[])
{

 int f, count;

 if (argc < 2) {
    return -1;
 }

 uid_t ruid, euid;
 gid_t egid, rgid;

 ruid = getuid();
 euid = geteuid();
 rgid = getgid();
 egid = getegid();

 printf("RUID: %d, EUID: %d, EGID:%d, RGID:%d\n",ruid, euid, egid, rgid);

 char buffer[2048];
 f = open(argv[1], O_RDONLY);
 

 while((count=read(f,buffer,sizeof(buffer)))>0) {
    printf("%s",buffer);
 }

 return 0;

}
