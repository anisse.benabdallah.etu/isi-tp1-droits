#!/bin/sh

sudo groupadd groupe_a
sudo groupadd groupe_b
sudo groupadd groupe_c

sudo adduser admiin --disabled-password --gecos GECOS
sudo adduser lambda_a --disabled-password --gecos GECOS
sudo adduser lambda_b --disabled-password --gecos GECOS


sudo usermod -a -G groupe_a,groupe_b,groupe_c admiin
sudo usermod -g groupe_a lambda_a
sudo usermod -g groupe_b lambda_b


mkdir dir_a
mkdir dir_b
mkdir dir_c

sudo chown -R admiin:groupe_a ./dir_a
sudo chown -R admiin:groupe_b ./dir_b
sudo chown -R admiin:groupe_c ./dir_c


sudo -u admiin chmod 3770 dir_a
sudo -u admiin chmod 3770 dir_b
sudo -u admiin chmod 2771 dir_c



sudo -u admiin touch dir_a/file_a
sudo -u admiin touch dir_a/file_aa
sudo -u admiin mkdir dir_a/dir2_a
sudo -u admiin touch dir_a/dir2_a/file2_a

sudo -u admiin touch dir_b/file_b
sudo -u admiin touch dir_b/file_ba
sudo -u admiin mkdir dir_b/dir2_b
sudo -u admiin touch dir_b/dir2_b/file2_b


sudo -u admiin touch dir_c/file_c
sudo -u admiin touch dir_c/file_ca
sudo -u admiin mkdir dir_c/dir2_c
sudo -u admiin touch dir_c/dir2_c/file2_c