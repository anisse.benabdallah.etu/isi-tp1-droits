#!/bin/bash

file_b="dir_b/file_b"
 
 
if [ -r $file_b ] && [ -w $file_b ]
then
   echo "Write and Read permissions are granted on $file_b"
else
   echo "Write and Read permissions are NOT granted on $file_b"
fi


file2_b="dir_b/dir2_b/file2_b"

if [ -r $file2_b ] && [ -w $file2_b ]
then
   echo "Write and Read permissions are granted on $file2_b"
else
   echo "Write and Read permissions are NOT granted on $file2_b"
fi



mkdir "./dir_b/dir_b2" && echo "directory dir_b2 created !"

touch "./dir_b/file_b2" && echo "File file_b2 created !"


rm -d -f "./dir_b/dir_b2" && echo "directory dir_b2 deleted !"

rm -f "./dir_b/file_b2" && echo "File file_b2 deleted !"


mv $file_b dir_b/file_b2

mv $file2_b dir_b/dir2_b/file2_b2

rm $file_b 



file_c="dir_c/file_c"
 
 
if [ -r $file_c ]
then
   echo "Read permissions are granted on $file_c"
else
   echo "Read permissions are NOT granted on $file_c"
fi


if [ -w $file_c ]
then
   echo "Write permissions are granted on $file_c"
else
   echo "Write permissions are NOT granted on $file_c"
fi


file2_c="dir_c/dir2_c/file2_c"

if [ -r $file2_c ]
then
   echo "Read permissions are granted on $file2_c"
else
   echo "Read permissions are NOT granted on $file2_c"
fi


if [ -w $file2_c ]
then
   echo "Write permissions are granted on $file2_c"
else
   echo "Write permissions are NOT granted on $file2_c"
fi


mv $file_c dir_c/file22_c 

mv $file2_c dir_c/dir2_c/file22_c 

rm -f $file_c  

rm -f $file2_c  

touch dir_c/file22_c



file_a="dir_a/file_a"

file2_a="dir_a/dir2_a/file2_a"


if [ -r $file_a ]
then
   echo "Read permissions are granted on $file_a"
else
   echo "Read permissions are NOT granted on $file_a"
fi


if [ -w $file2_a ] 
then
   echo "Write permissions are granted on $file2_a"
else
   echo "Write permissions are NOT granted on $file2_a"
fi



mv $file_a dir_a/file22_a 

mv $file2_a dir_a/dir2_a/file22_a 

rm -f $file_a  

rm -f $file2_c  

touch dir_a/file22_a