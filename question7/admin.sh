#!/bin/bash


file_c="dir_c/file_c"

file2_c="dir_c/dir2_c/file2_c"


if [ -w $file_c ] && [ -w $file2_c ]
then
   echo "Write permissions are granted on dir_c"
else
   echo "Write permissions are NOT granted on dir_c"
fi


touch "dir_c/file_c2" && echo "file dir_c/file_c2 created !"


mv dir_a/file_aa dir_a/file_aa2  && echo "file dir_a/file_aa renamed !"
mv dir_b/file_ba dir_b/file_ba2  && echo "file dir_b/file_ba renamed !"
mv dir_c/file_ca dir_c/file_ca2  && echo "file dir_c/file_ca renamed !"


rm -f dir_a/file_aa2 && echo "file file_aa2 deleted !"
rm -f dir_b/file_ba2 && echo "file file_ab2 deleted !"
rm -f dir_c/file_ca2 && echo "file file_ca2 deleted !"