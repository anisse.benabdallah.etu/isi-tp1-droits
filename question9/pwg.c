#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>
#include <time.h>
#include <crypt.h>

#include "check_pass.h"

int main(int argc, char *argv[])
{

    int f, count;

    if (argc < 1) {
        return -1;
    }

    // get user name

    uid_t ruid = getuid();
    struct passwd *pw = getpwuid(ruid);

    // Enter password
    char passewd[20];
    char *salt = "50";

    if (have_passw(pw->pw_name)==1){
        printf("enter passwd: ");
        scanf("%s", passewd);
        // Check password and change it

        if (check_new_pass(pw->pw_name, passewd) == 1){
            char newpassewd[20];
            printf("enter new passwd: ");
            scanf("%s", newpassewd);
            // Crypte 
            char *encrypted = crypt(passewd, salt);
            char string[1024];
            sprintf(string, "sudo -u admiin sed -i 's/%s/%s/g' /home/admiin/passwd  > temp.txt",passewd,encrypted);
            system(string);
            system("sudo -u admiin rm /home/admiin/passwd");
            system("sudo -u admiin mv temp.txt/home/admiin/passwd");
        }
        else{
            printf("Wrong passwd ! \n");
            exit(EXIT_FAILURE);
        }
    }
    else {
        printf("create passwd: ");
        scanf("%s", passewd);
        char *encrypted = crypt(passewd, salt);
        create_passwd(pw->pw_name, encrypted);
    }
    

    return 0;

}