#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <pwd.h>

#include "check_pass.h"

int main(int argc, char *argv[])
{

    int f, count;

    if (argc < 2) {
        return -1;
    }

    // Get file groupe id

    struct stat sb;
    stat(argv[1], &sb);
            
    // Get user group id

    gid_t gid = getgid();

    // get user name

    uid_t ruid = getuid();
    struct passwd *pw = getpwuid(ruid);


    // Check group permission

    if (sb.st_gid != gid ){
        printf("User dont have permission ! \n");
        exit(EXIT_FAILURE);
    }


    // Enter password

    char passewd[20];
    printf("Enter passwd: ");
    scanf("%s", passewd);

    
    // Check password and remove file

    if (check_pass(pw->pw_name, passewd) == 1){
        printf("Correct !\n");
        if (remove(argv[1])==0){
            printf("The file is deleted successfully.\n");
            exit(EXIT_SUCCESS);
        }
        else {
        printf("The file is not deleted.\n");
        }
    }
    else{
        printf("Wrong ! \n");
        printf("Can't Delete File !\n");
        exit(EXIT_FAILURE);
    }

    return 0;

}