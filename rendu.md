# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- BENABDALLAH, Anisse
- SALMI, Anis

## Question 1

- creation de l'utilsateur toto :

```shell
sudo useradd -g ubuntu toto
```

```
touch file.txt
chmod u-w file.txt
```
-  Le processus ne peut pas écrire, car l'utilisateur n'a pas les droits d'écriture sur ce fichier. Le fichier est ouvert seulement en lecture pour le propriétaire.


## Question 2

- Pour les répertoires le caractère x signifie qu'on peut avoir acces à ce répertoire et à tout ses sous-répertoires.

```
mkdir mydir 
chmod g-x mydir/
```

- L'utilisateur toto ne peut pas acceder au repértoire, car les utilsateurs du groupe ne peuvent pas y'avoir acces.

```
touch mydir/data.txt
sudo su toto
ls -al mydir
```

```
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt
```

- On constate que l'utilsateur toto peut voir les noms des fichier, mais ne peut pas voir les détails car il n'a pas les permission pour executer le repertoire.


## Question 3

```
gcc -c suid.c
gcc suid.c -o suid
./suid mydir/data.txt 
```

- Avec l'utilisateur ubuntu Le processus arrive a ouvrir le fichier en lecture, on a :

```
RUID: 1000, EUID: 1000, EGID:1000, RGID:1000
je suis ouvert en lecture
```


- Avec l'utilisateur toto Le processus n'arrive pas a ouvrir le fichier en lecture, on a :

```
RUID: 1001, EUID: 1001, EGID:1000, RGID:1000
```


- Aprés avoir activé le flag set-user-id du fichier exécutable

```
chmod u+s ./suid
```

```
./suid mydir/data.txt
RUID: 1001, EUID: 1000, EGID:1000, RGID:1000
je suis ouvert en lecture
```

- On constate que l'utilisateur toto arrive a ouvrir le fichier en lecture.

## Question 4

```
python3 suid.py 
chmod u+s suid.py
```

```
python3 suid.py
EUID 1001
EGID 1000
```

## Question 5

- La commande ls chfn sert à modifier les informations d'un utilisateur (full name, cabinet number, office number and home phone number for the user account).

```
$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

- Le fichier est ouvert en lecture, ecriture pour les utilisateurs 


## Question 6

- Les mots de passes des utilisateurs sont stockés dans un fichier /etc/shadow, qui est accesible uniquement par le root.

```
$ cat /etc/shadow
cat: /etc/shadow: Permission denied
```

## Question 7

- Pour etre sur que les users et les groupes n'existe pas déja on execute d'abbord ce script qui va nettoyer, dans le répertoire `question7`:

```
sh clear.sh
```

- On va créer les users et les groupes ainsi que l'utilsateur admiin, puis on va créer les répertoires demandés avec les bonnes pérmissions :

```
sh create.sh
```

- Pour tester, on peut lancer les scripts suivant pour les différents users de chaque groupes :

```
sudo su lambda_a user_a.sh 
sudo su lambda_b user_b.sh
sudo su admiin admin.sh 
```


- Pour renettoyer le répértoire (pour la question 8): 

```
sh clear.sh
```


## Question 8

Dans le répertoite `Question8` :

- Pour pouvoir recréer les répertoire et les utilisateurs (de la qeustion 7), on peut lancer :

```
sh ../question7/create.sh
```


- Pour creer les fichier de tests dans les repertoires dir_a et dir_b, et pour créer le fichier `passwd`, lancez ce script avec :

```
sudo su admiin admin.sh
```

- Le fichier `passwd` contient les username et les passwords des users sous cette form :

```
lambda_a:user_a
lambda_b:user_b 
```

- Pour compiler le programme C :

```
make rmg
```

- Pour lancer les tests :

```
sh rmg.sh 
```


## Question 9

Dans le répertoite `Question9` :

- Pour pouvoir recréer les répertoire et les utilisateurs (de la qeustion 7), on peut lancer :

```
sh ../question7/create.sh
```


- Pour creer les fichier de tests dans les repertoires dir_a et dir_b, et pour créer le fichier `passwd`, lancez ce script avec :

```
sudo su admiin admin.sh
```

- Pour compiler le programme C :

```
make 
```

- Pour lancer les tests :

```
sh pwg.sh 
```









