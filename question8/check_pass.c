#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int check_pass(char* user, char* passwd) {

    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("/home/admiin/passwd", "r");
    if (fp == NULL){
        printf("KO");
        exit(EXIT_FAILURE);
    }


    while ((read = getline(&line, &len, fp)) != -1) {

        // Remove "\n"
        len = strlen(line);
        if( line[len-1] == '\n' )
            line[len-1] = 0;

        // Split Line in ":"
        char * token = strtok(line, ":");

        // Compare credentials
        if (strcmp(token, user)==0){
            token = strtok(NULL, " ");
            // check passwd
            if (strcmp(token, passwd)==0){
                return 1;
            }
            // worong passwd
            else{
                return 0;
            }
        }
    }

    // Wrong credentials
    fclose(fp);
    return 0;
}