#!/bin/bash

# Attribution des droits pour l'executable rmg 

sudo chown admiin:admiin rmg
sudo chmod u+s rmg


# Lancement des tests

echo "user lambda_a want to delete dir_a/file_a8, with wrong passe word"
printf 'user_b\n' | sudo -u lambda_a ./rmg dir_a/file_a8 

echo "\n"

echo "user lambda_a want to delete dir_a/file_a8, with correct passe word"
printf 'user_a\n' |sudo -u lambda_a ./rmg dir_a/file_a8 


echo "\n"

echo "user lambda_b want to delete dir_b/file_b8, with wrong passe word"
printf 'user_a\n' | sudo -u lambda_b ./rmg dir_b/file_b8 

echo "\n"

echo "user lambda_b want to delete dir_b/file_b8, with correct passe word"
printf 'user_b\n' |sudo -u lambda_b ./rmg dir_b/file_b8 